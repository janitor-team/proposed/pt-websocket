DESTDIR =
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
VERSION = 0.1

GOBUILDFLAGS =
# Alternate flags to use gccgo, allowing cross-compiling for x86 from
# x86_64, and presumably better optimization. Install this package:
#   apt-get install gccgo-multilib
# GOBUILDFLAGS = -compiler gccgo -gccgoflags "-O3 -m32 -static-libgo"

all: pt-websocket-server/pt-websocket-server

pt-websocket-server/pt-websocket-server: pt-websocket-server/*.go websocket/*.go
	cd pt-websocket-server && go build $(GOBUILDFLAGS)

pt-websocket-client/pt-websocket-client: pt-websocket-client/*.go
	cd pt-websocket-client && go build $(GOBUILDFLAGS)

doc/pt-websocket-server.1: pt-websocket-server/pt-websocket-server
	help2man --no-info --name "WebSocket server pluggable transport" --version-string "$(VERSION)" -o "$@" "$<"

install: pt-websocket-server/pt-websocket-server
	mkdir -p "$(DESTDIR)$(BINDIR)"
	cp -f "$<" "$(DESTDIR)$(BINDIR)"

clean:
	rm -f pt-websocket-server/pt-websocket-server pt-websocket-client/pt-websocket-client
	rm -f doc/pt-websocket-server.1

fmt:
	go fmt ./pt-websocket-server ./pt-websocket-client ./websocket

.PHONY: all install clean fmt
